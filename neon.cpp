#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <map>

using namespace std;

struct para;

para zwrocNajzdluzszaPare(para p[4]);
void znajdzNajdluzszy(ifstream &dataIn, ofstream &dataOut);
void wyszukajIZastap(string &s, char arg);
string zlozSlowo(ifstream &dataIn);
void znajdzLitereDlaDopisz(ifstream &dataIn, ofstream &dataOut);

int main()
{
    ifstream dataIn;
    ofstream dataOut("wyniki4.txt");

    dataIn.open("./DANE_2105/instrukcje.txt");
    if (!dataIn.is_open())
        return -1;

    string slowo = zlozSlowo(dataIn);

    dataOut << "1) " << slowo.length() << endl;
    znajdzNajdluzszy(dataIn, dataOut);
    znajdzLitereDlaDopisz(dataIn, dataOut);
    dataOut << "4) " << slowo << endl;
    dataIn.clear();

    return 0;
}

struct para
{
    string nazwa;
    int iloscWystapien = 0;
};

void znajdzLitereDlaDopisz(ifstream &dataIn, ofstream &dataOut)
{

    string instr, arg;
    para dopiszArg;
    map<string, int> m;
    int liczArg = 0;

    dataIn.seekg(0, ios::beg);

    while (!dataIn.eof())
    {
        dataIn >> instr >> arg;

        if ("DOPISZ" == instr)
        {
            map<string, int>::iterator it = m.find(arg);

            if (it == m.end())
                m.insert(pair<string, int>(arg, 1));
            else
            {
                m.at(arg) = ++(it->second);
            }
        }
    }

    map<string, int>::iterator itr;
    int max = 0;
    string maxName;

    for (itr = m.begin(); itr != m.end(); itr++)
    {
        if (itr->second > max)
        {
            maxName = itr->first;
            max = itr->second;
        }
    }

    dataOut << "3) " << maxName << " " << max << endl;
    dataIn.clear();
}

para zwrocNajzdluzszaPare(para p[4])
{
    para max = p[0];
    for (int i = 0; i < 4; i++)
    {
        if (p[i].iloscWystapien > max.iloscWystapien)
            max = p[i];
    }

    return max;
}

void znajdzNajdluzszy(ifstream &dataIn, ofstream &dataOut)
{
    string instr;
    char arg;
    int liczDopisz = 0, liczUsun = 0, liczZmien = 0, liczPrzesun = 0;
    para dopisz, usun, zmien, przesun;

    dopisz.nazwa = "DOPISZ";

    usun.nazwa = "USUN";

    zmien.nazwa = "ZMIEN";

    przesun.nazwa = "PRZESUN";

    dataIn.seekg(0, ios::beg);
    while (!dataIn.eof())
    {
        dataIn >> instr >> arg;
        if (instr == dopisz.nazwa)
        {
            liczDopisz++;
            if (liczDopisz > dopisz.iloscWystapien)
                dopisz.iloscWystapien = liczDopisz;
        }
        else
            liczDopisz = 0;

        if (instr == usun.nazwa)
        {
            liczUsun++;
            if (liczUsun > usun.iloscWystapien)
                usun.iloscWystapien = liczUsun;
        }
        else
            liczUsun = 0;

        if (instr == zmien.nazwa)
        {
            liczZmien++;
            if (liczZmien > zmien.iloscWystapien)
                zmien.iloscWystapien = liczZmien;
        }
        else
            liczZmien = 0;

        if (instr == przesun.nazwa)
        {
            liczPrzesun++;
            if (liczPrzesun > przesun.iloscWystapien)
                przesun.iloscWystapien = liczPrzesun;
        }
        else
            liczPrzesun = 0;
    }

    para arr[4] = {usun, zmien, przesun, dopisz};
    para max = zwrocNajzdluzszaPare(arr);
    dataOut << "2) " << max.nazwa << " " << to_string(max.iloscWystapien) << endl;
    dataIn.clear();
}

void wyszukajIZastap(string &s, char arg)
{
    int indeks = s.find(arg);

    if (indeks != string::npos)
    {
        arg++;
        if (arg > 90)
            s[indeks] = arg - 26;
        else
            s[indeks] = arg;
    }
}

string zlozSlowo(ifstream &dataIn)
{
    dataIn.seekg(0, ios::beg);

    string dyrektywa;
    char argument;
    string slowo = "";

    while (!dataIn.eof())
    {
        dataIn >> dyrektywa >> argument;
        if (dyrektywa == "DOPISZ")
        {
            slowo += argument;
        }
        else if (dyrektywa == "USUN")
        {
            slowo.erase(slowo.length() - 1);
        }
        else if (dyrektywa == "ZMIEN")
        {
            slowo[slowo.length() - 1] = argument;
        }
        else if (dyrektywa == "PRZESUN")
        {
            wyszukajIZastap(slowo, argument);
        }
    }

    dataIn.clear();
    return slowo;
}
